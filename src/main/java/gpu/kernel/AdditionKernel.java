package gpu.kernel;

class AdditionKernel extends TwoInputOneResultKernel {

	AdditionKernel(KernelType kernelType) {
		super(kernelType);
	}

	@Override
	public void run() {
		if (result.length > 0) {
			int i = getGlobalId();
			result[i] = x[i] + y[i];
		}
	}
}
