package gpu.kernel;

import com.aparapi.Kernel;
import com.aparapi.Range;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public abstract class BasicKernel extends Kernel {

	private final KernelType kernelType;
	private Range range;

	private final List<Thread> threadList = new ArrayList<>();

	BasicKernel(KernelType kernelType) {
		super();
		this.kernelType = kernelType;
	}

	public void executeWithRange(int range, boolean async) {
		this.range = Range.create(range);
		executeWithRange(this.range, async);
	}

	public void executeWithPreviousRange(boolean async) {
		if (this.range == null) {
			this.range = Range.create(1);
		}
		executeWithRange(this.range, async);
	}

	private void executeWithRange(Range range, boolean async) {
		if (async) {
			executeAsyc(range);
		} else {
			LocalDateTime begin = LocalDateTime.now();
			execute(range);
			LocalDateTime end = LocalDateTime.now();
			System.out.println("Kernel: " + kernelType.name() +
					" GPU: " + ChronoUnit.MILLIS.between(begin, end) + " ms " +
					getTargetDevice().getType() +
					getTargetDevice().getShortDescription());
		}
	}

	public void executeOnce(boolean async) {
		executeWithRange(1, async);
	}

	private void executeAsyc(Range range) {
		Thread thread = new Thread(() -> {
			LocalDateTime begin = LocalDateTime.now();
			execute(range);
			LocalDateTime end = LocalDateTime.now();
			System.out.println("Kernel: " + kernelType.name() +
					" GPU Async: " + ChronoUnit.MILLIS.between(begin, end) + " ms " +
					getTargetDevice().getType() + " " +
					getTargetDevice().getShortDescription());
		});
		thread.start();
		threadList.add(thread);
	}

	boolean isAsyncExecuting() {
		return threadList.stream().anyMatch(Thread::isAlive);
	}

	public abstract void setParams(float[] x);

	public abstract void setParams(float[] x, float[] y);

	public List<Thread> getThreadList() {
		return threadList;
	}

	public void cleanUp() {
		threadList.clear();
	}

	KernelType getKernelType() {
		return kernelType;
	}
}
