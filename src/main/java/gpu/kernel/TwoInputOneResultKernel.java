package gpu.kernel;

abstract class TwoInputOneResultKernel extends OneInputOneResultKernel{

	float[] y = new float[0];

	TwoInputOneResultKernel(KernelType kernelType) {
		super(kernelType);
	}

	@Override
	public void setParams(float[] x) {
		throw new RuntimeException();
	}

	@Override
	public void setParams(float[] x, float[] y) {
		super.setParams(x);
		if (y == null || x.length != y.length) {
			throw new IllegalArgumentException();
		}
		this.y = y;
	}
}
