package gpu.kernel;

abstract class OneInputOneResultKernel extends BasicKernel {
	float[] result = new float[0];
	float[] x = new float[0];

	OneInputOneResultKernel(KernelType kernelType) {
		super(kernelType);
	}

	public float[] getResult() {
		return result;
	}

	@Override
	public void setParams(float[] x, float[] y) {
		throw new RuntimeException();
	}

	@Override
	public void setParams(float[] x) {
		if (x == null) {
			throw new IllegalArgumentException();
		}
		this.x = x;
		result = new float[x.length];
	}
}
