package gpu.kernel;

class ComplexKernel extends TwoInputOneResultKernel{

	ComplexKernel(KernelType kernelType) {
		super(kernelType);
	}

	@Override
	public void run() {
		if (result.length > 0) {
			int i = getGlobalId();
			result[i] = (float) Math.sin(Math.sqrt(x[i] * x[i] + y[i] * y[i]) * 20) +
					(float) Math.cos(Math.sqrt(x[i] * x[i] + y[i] * y[i]) * 20) * 2;
		}
	}
}
