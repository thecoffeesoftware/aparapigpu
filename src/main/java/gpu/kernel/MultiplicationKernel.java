package gpu.kernel;

public class MultiplicationKernel extends TwoInputOneResultKernel{

	MultiplicationKernel(KernelType kernelType) {
		super(kernelType);
	}

	@Override
	public void run() {
		if (result.length > 0) {
			int i = getGlobalId();
			result[i] = x[i] * y[i];
		}
	}
}
