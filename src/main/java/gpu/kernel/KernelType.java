package gpu.kernel;

public enum KernelType {
	ADDITION,
	COMPLEX,
	MULTIPLICATION,
	SORT
}
