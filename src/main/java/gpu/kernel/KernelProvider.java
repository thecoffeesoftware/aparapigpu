package gpu.kernel;

import com.aparapi.Kernel;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static gpu.kernel.KernelType.ADDITION;
import static gpu.kernel.KernelType.COMPLEX;
import static gpu.kernel.KernelType.MULTIPLICATION;
import static gpu.kernel.KernelType.SORT;

public class KernelProvider {

	private KernelProvider() {
	}

	private static List<BasicKernel> kernelList = new ArrayList<>();

	public static BasicKernel provide(KernelType kernelType) {
		BasicKernel kernel = getKernel(kernelType).orElseThrow(IllegalArgumentException::new);
		initialExecute(kernel);
		kernelList.add(kernel);
		return kernel;
	}

	private static void initialExecute(BasicKernel kernel) {
		System.out.println(kernel.toString());
		LocalDateTime begin = LocalDateTime.now();
		kernel.executeWithRange(1, false);
		LocalDateTime end = LocalDateTime.now();
		System.out.println("Kernel: " + kernel.getKernelType().name() + " GPU initialization: " + ChronoUnit.MILLIS.between(begin, end) + "ms");
	}

	public static boolean isAnyRunning() {
		return kernelList.stream().anyMatch(BasicKernel::isAsyncExecuting);
	}

	public static void disposeAll() {
		kernelList.forEach(Kernel::dispose);
		kernelList = new ArrayList<>();
	}

	public static void dispose(BasicKernel kernel) {
		kernelList.remove(kernel);
		kernel.dispose();
	}

	private static Optional<BasicKernel> getKernel(KernelType kernelType) {
		switch (kernelType) {
			case ADDITION:
				return Optional.of(new AdditionKernel(ADDITION));
			case SORT:
				return Optional.of(new SortKernel(SORT));
			case COMPLEX:
				return Optional.of(new ComplexKernel(COMPLEX));
			case MULTIPLICATION:
				return Optional.of(new MultiplicationKernel(MULTIPLICATION));
		}
		return Optional.empty();
	}

	public static void waitAll(int checkPeriod) throws InterruptedException {
		while (isAnyRunning()) {
			Thread.sleep(checkPeriod);
		}
		kernelList.forEach(BasicKernel::cleanUp);
	}
}
