import gpu.kernel.BasicKernel;
import gpu.kernel.KernelProvider;
import gpu.kernel.KernelType;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Application {

	private static int size = 1000000;
	private static int threadCount = 15;
	private static final List<Thread> threadList = new ArrayList<>();
	private static int checkPeriod = 1;

	public static void main(String[] args) throws InterruptedException {
	compareAddition();
	KernelProvider.waitAll(checkPeriod);
	KernelProvider.disposeAll();
	}

	public static void waitAll(int checkPeriod) throws InterruptedException {
		while (isAsyncExecuting()) {
			Thread.sleep(checkPeriod);
		}
		threadList.clear();
	}

	static boolean isAsyncExecuting() {
		return threadList.stream().anyMatch(Thread::isAlive);
	}

	private static void compareAddition() throws InterruptedException {
		float[] x = generateRandomArray(size);
		float[] y = generateRandomArray(size);
		cpuAddition(x, y);
		gpuAddition(x, y);
		cpuAdditionAsync(x, y, threadCount);
		gpuAdditionAsync(x, y, threadCount);
	}

	private static void cpuAddition(float[] x, float[] y){
		addition(x, y, "");
	}

	private static void cpuAdditionAsync(float[] x, float[] y, int threadCount) throws InterruptedException {
		LocalDateTime begin = LocalDateTime.now();
		for(int i = 0; i < threadCount; i++){
			Thread t = new Thread(() -> addition(x, y, " Async "));
			t.start();
			threadList.add(t);
		}
		waitAll(checkPeriod);
		LocalDateTime end = LocalDateTime.now();
		System.out.println("threads ADDITION CPU: " + ChronoUnit.MILLIS.between(begin, end) + "ms");
	}

	private static void addition(float[] x, float[] y, String msg){
		LocalDateTime begin = LocalDateTime.now();
		float[] result = new float[size];
		for(int i = 0; i < result.length; i++){
			result[i] = x[i] + y[i];
		}
		LocalDateTime end = LocalDateTime.now();
		System.out.println("ADDITION" + msg + "CPU: " + ChronoUnit.MILLIS.between(begin, end) + "ms");
	}

	private static void gpuAddition(float[] x, float[] y){
		BasicKernel kernel = KernelProvider.provide(KernelType.ADDITION);
		kernel.setParams(x,y);
		kernel.executeWithRange(size, false);
		KernelProvider.dispose(kernel);
	}

	private static void gpuAdditionAsync(float[] x, float[] y, int thread) throws InterruptedException {
		BasicKernel kernel = KernelProvider.provide(KernelType.ADDITION);
		kernel.setParams(x,y);
		System.out.println("GPU async thread count: " + thread);
		LocalDateTime begin = LocalDateTime.now();
		for(int i = 0; i < thread; i ++) {
			kernel.executeWithRange(size, true);
		}
		KernelProvider.waitAll(checkPeriod);
		LocalDateTime end = LocalDateTime.now();
		System.out.println("threads GPU: " + ChronoUnit.MILLIS.between(begin, end) + "ms");
		KernelProvider.dispose(kernel);
	}

	private static float[] generateRandomArray(int size){
		final float[] a = new float[size];
		for (int i = 0; i < a.length; i++) {
			a[i] = (float) (Math.random() * 100);
		}
		return a;
	}
}
